#!/bin/sh

cresult=$(npm ls -parseable 2>&1)

# source : https://stackoverflow.com/a/24753942
case "$cresult" in
*ERR\!\ missing*) 
	echo "docker-start.sh : Fresh start or a least one lib is missing. 'npm install' will run to init the environment"
    npm install
    echo "docker-start.sh : End of initial install"
	;;
esac

echo "docker-start.sh : Starting the Gatsby server"
gatsby develop -H 0.0.0.0
# top

